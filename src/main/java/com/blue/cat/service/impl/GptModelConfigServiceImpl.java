package com.blue.cat.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blue.cat.bean.entity.GptModelConfig;
import com.blue.cat.mapper.GptModelConfigMapper;
import com.blue.cat.service.IGptModelConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lixin
 * @since 2023-08-01
 */
@Service
public class GptModelConfigServiceImpl extends ServiceImpl<GptModelConfigMapper, GptModelConfig> implements IGptModelConfigService {

}
