package com.blue.cat.bean.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author lixin
 * @since 2023-08-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("gpt_model_config")
public class GptModelConfig implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 访问的地址
     */
      private Long id;

    private String baseUrl;

    /**
     * 访问地址
     */
    private String token;

    /**
     * 模型标识
     */
    private String model;

    /**
     * 权重
     */
    private Integer weight;

    /**
     * 状态 1：开启 0 ：禁用
     */
    private Integer status;

    /**
     * 新增时间
     */
    private LocalDateTime createTime;

    private String createUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新人
     */
    private String updateUser;


}
