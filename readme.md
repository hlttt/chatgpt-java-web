 ### 使用Java语言作为后端对接 chatgpt，使用简单，并且有可以免费chatgpt3.5,没有套路
 ### 后端架构：mysql,springBoot 部署简单维护也简单
 ###蓝猫AI后端服务指引
### 效果可以体验 ： http://www.chosen1.xyz/
### 对应前端地址： https://gitee.com/lixinjiuhao/chatgpt-web-java
 ### 如果没有合适的代理地址可以走我这边的代理，免费
 #### 步骤1：
    在蓝猫AI http://www.chosen1.xyz/ 注册账号
 #### 步骤2： 获取系统的token
 ![系统的token](image/config.jpeg)
 
  #### 步骤3： 设置代理地址和token
    国内环境中：http://www.chosen1.xyz/ + token(蓝猫AI系统中的token)
    海外环境走：https://www.liulinlin.top + token(蓝猫AI系统中的token)
    调用方式和原生的gpt的api调用是一样的，目前蓝猫AI支持全模型的调用（gpt-3.5 gpt-4 gpt-4-32k）
    如果需要购买gpt额度，可以找蓝猫AI的管理员，价格价（具体参考蓝猫AI系统的定价）如下：
    1美元 = 2.5元 额度买的多越便宜
 
## 后端定制化返回markdowm格式给前端进行图片展示：
![提示词]("url")

##上效果图
![pc端的黑色背景聊天图](image/pc-chat-black.jpg)
![pc端的黑白色背景聊天图](image/image/pc-chat-white.jpg)
![pc端prompt工具图](image/pc-tool.jpg)
![手机端的黑色背景聊天图](image/phone-chat-black.jpg)
![手机端的白色背景聊天图](image/phone-chat-white.jpg)
![手机端的prompt工具图](image/phton-tool.jpg)

## 项目启动准备
### 本地或者线上环境的 mysql连接信息
![项目启动前必填](image/项目启动前必填.jpeg)
2023-08-10开启由数据库表管理 gpt配置，具体如下图，支持各种中转站的key
![项目启动前必填](image/项目启动前必填1.jpeg)
### 本地根据vm指令去加载不同环境的配置
#### 步骤一
![步骤一](image/idea的vm配置步骤1.jpeg)
#### 步骤二
![步骤二](image/idea的vm配置步骤2.jpeg)
解释说明
按照上面操作：
vm options添加： -Dspring.profiles.active=dev 则代表 加载 application-dev.yml 配置
vm options添加： -Dspring.profiles.active=prod 则代表 加载 application-prod.yml 配置
对应下图的
![步骤二](image/vm配置解释说明.jpeg)

# 项目部署 方式1：
 ##服务器中运行脚本： 
    java -jar xxx.jar --spring.profiles.active=dev
 ## 本地运行：
     线上环境
     vm option添加：-Dspring.profiles.active=prod
     nohup java -jar -Xmx512m -Xms512m -XX:MaxPermSize=256m -XX:PermSize=128m -XX:MetaspaceSize=256M  -XX:MaxMetaspaceSize=256M  -XX:+UseParallelGC -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:/var/log/myapp/gc.log  blue-cat-0.0.8-SNAPSHOT.jar --spring.profiles.active=prod 
     本地环境
     vm option添加：-Dspring.profiles.active=dev

## maven打包命令
    mvn clean install -U -Dmaven.test.skip=true

# 项目部署部署 方式2
## 通过脚本一键部署 
### 步骤一：
    前提： 需要在服务器安装 git , maven ， jdk  
    脚本做了以下几件事：
    1. 从git拷贝项目（这个步骤最好是手动执行，因为一般来说git需要登录，可以使用下面的命令来记录git账号密码）
    git config --global credential.helper store
    2. 拷贝完项目之后将会在项目的根目录下 执行：git pull
    3. 然后开始使用maven打包项目
    4. 通过jdk启动打包好的jar
### 步骤二：
    打开目标脚本： bin/server.sh  ，按照你自己的配置进行改动脚本里面的自定义脚本配置
   ![脚本需要关心的配置](image/脚本配置.jpeg)
### 步骤三：
    拷贝server.sh 到项目所需要保存的位置
    执行sh server.sh start // 启动命令 
    执行sh server.sh restart //重启项目
    执行sh server.sh stop // 停止项目 
   ![脚本效果图](image/脚本启动成功效果图.jpeg)

   
# 疑问
    如果还有什么不清晰的，可以提 issues  ，希望大家点点 star

    加V一起交流
#  商用版本说明
    免费帮忙部署整套蓝猫AI服务，不需要任何服务费，唯一要求：gpt额度 找我们拿
    gpt额度远远低于市场价格，买贵了包退差价   
    
![联系我](image/me.jpg )
 
